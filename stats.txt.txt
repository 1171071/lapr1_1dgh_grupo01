public static void stats(double[][] image1) {

        double soma = 0, media;
        for (int i = 0; i < image1.length; i++) {
            for (int j = 0; j < image1.length; j++) {
                soma += image1[i][j];

            }
            media = (soma / image1.length);
            System.out.println("================================= LINHAS =================================\n");
            System.out.println("LINHA: " + i);
            System.out.println("Soma= " + soma);
            System.out.println("M�dia= " + media + "\n");

            soma = 0;
        }

        for (int i = 0; i < image1.length; i++) {
            for (int j = 0; j < image1.length; j++) {
                soma += image1[j][i];

            }
            media = (soma / image1.length);
            System.out.println("================================= COLUNAS =================================\n");
            System.out.println("COLUNA:" + i);
            System.out.println("Soma= " + soma);
            System.out.println("M�dia= " + media+"\n    ");
            soma = 0;
        }
        for (int i = 0; i < image1.length; i++) {
            for (int j = 0; j < image1.length; j++) {
                soma += image1[i][j];
            }
        }
        media = soma / (image1.length * image1.length);
        System.out.println("================================= GERAL =================================\n");
        System.out.println("A soma de todos os elementos � " + soma);
        System.out.println("A m�dia de todos os elementos � " + media);
    }


