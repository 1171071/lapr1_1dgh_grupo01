package digital.imaging.processing;

import static digital.imaging.processing.Menu_2.nomeFich;
import static digital.imaging.processing.DIP.in;
import static digital.imaging.processing.Utilities.saving;
import java.io.FileNotFoundException;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Menu_3 {

    public static int[][] transformations(Matrix a, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException {
        int op = 0;
        do {
            System.out.printf("%n%55s", "Transformations");
            System.out.println("\n=======================================================================================\n");
            System.out.println("What transformation do you wish to apply?");
            System.out.println(" 1 - Rotate 90º Clockwise \n 2 - Sort Each Column \n 0 - Exit");
            System.out.print("\nSelect an option: ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    System.out.println("Do you wish to rotate it clockwise (1) or anticlockwise (2)?");
                    op = in.nextInt();
                    in.nextLine();
                    System.out.printf("%n%44s", "Rotation");
                    System.out.println("\n=======================================================================================\n");
                    nova = rotation(a, nEl, nova, cont1, var, op);
                    break;
                case 2:
                    System.out.printf("%n%44s", "Sort");
                    System.out.println("\n=======================================================================================\n");
                    nova = sort(a, nEl, nova, cont1, var);
                    break;
                case 0:
                    System.out.println();
                    break;
                default:
                    break;
            }
            cont1++;
        } while (op != 0);
        return nova;
    }

    public static int[][] rotation(Matrix a, int nEl, int[][] nova, int cont1, boolean var, int op) throws FileNotFoundException {
        double[][] image2 = a.toDenseMatrix().toArray();
        int[][] image = new int[nEl][nEl];
        for (int p = 0; p < image2.length; p++) {
            for (int q = 0; q < image2.length; q++) {
                image[p][q] = (int) image2[p][q];
            }
        }
        int[][] rot = new int[nEl][nEl];
        Matrix mat = Utilities.convertToMatrix(rot);
        char res = 0;
        if (cont1 != 0) {
            System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
            res = in.next().charAt(0);
            if (res == 'Y') {
                rot = Utilities.rotate(nEl, image, op);
                Utilities.printMatrix(rot);
                mat = Utilities.convertToMatrix(rot);
                nomeFich = nomeFich + "Rotate_90";
                nova = saving(mat, res, cont1, image, nomeFich, var);
            }
            if (res == 'N') {
                rot = Utilities.rotate(nEl, nova, op);
                Utilities.printMatrix(rot);
                mat = Utilities.convertToMatrix(rot);
                nomeFich = nomeFich + "Rotate_90";
                nova = saving(mat, res, cont1, image, nomeFich, var);
            }
        }
        if (cont1 == 0) {
            rot = Utilities.rotate(nEl, image, op);
            Utilities.printMatrix(rot);
            mat = Utilities.convertToMatrix(rot);
            nomeFich = nomeFich + "Rotate_90";
            nova = saving(mat, res, cont1, image, nomeFich, var);
        }
        return nova;
    }

    public static int[][] sort(Matrix a, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException {
        int aux = 0;
        char res = 0;
        double[][] image2 = a.toDenseMatrix().toArray();
        int[][] image = new int[nEl][nEl];
        int[][] sorted = new int[nEl][nEl];
        for (int p = 0; p < image2.length; p++) {
            for (int q = 0; q < image2.length; q++) {
                image[p][q] = (int) image2[p][q];

            }
        }

        Matrix mat = Utilities.convertToMatrix(sorted);
        if (cont1 != 0) {
            System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
            res = in.next().charAt(0);
            if (res == 'Y') {
                for (int p = 0; p < image2.length; p++) {
                    for (int q = 0; q < image2.length; q++) {
                        sorted[p][q] = image[p][q];
                    }
                }
                for (int p = 0; p < image.length; p++) {
                    for (int g = 0; g < image.length; g++) {
                        for (int i = 0; i < image.length - 1; i++) {
                            if (sorted[i][g] < sorted[i + 1][g]) {
                                aux = sorted[i][g];
                                sorted[i][g] = sorted[i + 1][g];
                                sorted[i + 1][g] = aux;
                            }
                        }
                    }
                }
                mat = Utilities.convertToMatrix(sorted);
                Utilities.printMatrix(sorted);
                nomeFich = nomeFich + "Sort";
                nova = saving(mat, res, cont1, image, nomeFich, var);
            }

            if (res == 'N') {
                for (int p = 0; p < image2.length; p++) {
                    for (int q = 0; q < image2.length; q++) {
                        sorted[p][q] = nova[p][q];
                    }
                }
                for (int p = 0; p < image.length; p++) {
                    for (int g = 0; g < image.length; g++) {
                        for (int i = 0; i < image.length - 1; i++) {
                            if (sorted[i][g] < sorted[i + 1][g]) {
                                aux = sorted[i][g];
                                sorted[i][g] = sorted[i + 1][g];
                                sorted[i + 1][g] = aux;
                            }
                        }
                    }
                }
                mat = Utilities.convertToMatrix(sorted);
                Utilities.printMatrix(sorted);
                nomeFich = nomeFich + "Sort";
                nova = saving(mat, res, cont1, image, nomeFich, var);
            }
        }
        if (cont1 == 0) {
            for (int p = 0; p < image2.length; p++) {
                for (int q = 0; q < image2.length; q++) {
                    sorted[p][q] = image[p][q];
                }
            }
            for (int p = 0; p < image.length; p++) {
                for (int g = 0; g < image.length; g++) {
                    for (int i = 0; i < image.length - 1; i++) {
                        if (sorted[i][g] < sorted[i + 1][g]) {
                            aux = sorted[i][g];
                            sorted[i][g] = sorted[i + 1][g];
                            sorted[i + 1][g] = aux;
                        }
                    }
                }
            }
            mat = Utilities.convertToMatrix(sorted);
            Utilities.printMatrix(sorted);
            nomeFich = nomeFich + "Sort";
            nova = saving(mat, res, cont1, image, nomeFich, var);
        }
        return nova;
    }
}
