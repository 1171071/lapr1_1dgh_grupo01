package digital.imaging.processing;
import java.util.Scanner;

public class loadInMatrix {

    public static int[][] loading(Scanner finput, int nEl, int dim) {
        int j = 0;
        String line = finput.nextLine();
        String[] temp1 = line.split(" ");
        String dimensao = temp1[2];
        dim = (int) Double.parseDouble(dimensao);
        nEl = 2;
        int[][] image = new int[dim][dim];
        while (finput.hasNext() && nEl < dim + 2) {
            line = finput.nextLine();
            if (line.trim().length() > 0) {
                nEl = saveInMatrix(line, image, dim, nEl, j);
                j++;
            }
        }
        finput.close();
        return image;
    }

    public static int saveInMatrix(String line, int[][] image, int dim, int nEl, int j) {
        int num = 0;
        String[] strTemp = line.split(",");
        for (int i = 0; i < strTemp.length; i++) {
            String number = strTemp[i].trim();
            num = Integer.parseInt(number);
            if (num < 0 || num > 255) {
                System.out.println(num + " - Número Incorreto");
                break;
            }
            image[j][i] = num;
        }

        nEl++;
        return nEl;
    }

}
