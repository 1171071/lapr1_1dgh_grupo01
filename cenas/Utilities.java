package digital.imaging.processing;

import static digital.imaging.processing.DIP.in;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Utilities {

    public static int[][] expandMatrix(int[][] image, int nEl) {

        int[][] aux = new int[image.length + 2][image[0].length + 2];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                aux[i + 1][j + 1] = image[i][j];
            }
        }
        for (int j = 0; j < image[0].length; j++) {
            aux[0][j + 1] = image[0][j];
            aux[aux.length - 1][j + 1] = image[image.length - 1][j];
        }
        for (int i = 0; i < aux.length; i++) {
            aux[i][0] = aux[i][1];
            aux[i][aux[i].length - 1] = aux[i][aux[i].length - 2];
        }

        return aux;
    }

    public static Matrix storing(int op, double[][] array, int[] arr, int nEl, int i, int j, Matrix mat) {
        if (op == 3) {
            array[i - 1][j - 1] = arr[8];
        } else if (op == 4) {
            array[i - 1][j - 1] = arr[0];
        }

        return mat;
    }

    public static int[][] saving(Matrix mat, char res, int cont, int[][] image, String nomeFich, boolean var) throws FileNotFoundException {
        Matrix img = convertToMatrix(image);
        double novva[][] = mat.toDenseMatrix().toArray();
        int[][] nova = new int[image.length][image.length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                nova[i][j] = (int) novva[i][j];
            }
        }
        if (var == true) {
            System.out.println("\nDo you wish to save the changes? (Y/N)");
            res = in.next().charAt(0);
            System.out.println();
            if (res == 'Y') {
                try (Formatter out = new Formatter(new File("New" + nomeFich + ".txt"))) {
                    out.format("Original Matrix ");
                    out.format("\n");
                    out.format("%-8s", img);
                    out.format("\n");
                    out.format("New_Matrix" + nomeFich);
                    out.format("\n");
                    out.format("%-8s", mat);
                }
            }
        }
        return nova;
    }

    public static Matrix convertToMatrix(int[][] image) {
        double[][] img = new double[image.length][image.length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                img[i][j] = image[i][j];
            }
        }
        Matrix temp = new Basic2DMatrix(img);
        return temp;
    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] rotate(int nEl, int[][] image, int op) {
        int[][] rot = new int[nEl][nEl];
        if (op == 1) {
            for (int i = 0; i < nEl; i++) {
                for (int j = 0; j < nEl; j++) {
                    rot[i][j] = image[image.length - 1 - j][i];
                }
            }
        } else if (op == 2) {
            for (int i = 0; i < nEl; i++) {
                for (int j = 0; j < nEl; j++) {
                    rot[i][j] = image[j][image.length - 1 - i];
                }
            }
        }
        return rot;
    }
}
