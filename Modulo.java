package digital.imaging.processing;

public class DIP {

    public static void main(String[] args) {

        double[][] matriz = new double[10][10];
        double soma;
        int dim = 0, cont = 0;
        double[] pixels = new double[9];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                if (i == 0 && j == 0) {
                    for (int k = 0; k < 3; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j + 1];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i + 1][j];
                        cont++;
                    }
                    pixels[cont] = matriz[i + 1][j + 1];

                } else if (i == 0 && j == dim - 1) {
                    soma = matriz[i][j] * 3 + matriz[i][j - 1] * 2 + matriz[i - 1][j - 1] + matriz[i + 1][j] * 2;
                    for (int k = 0; k < 3; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j - 1];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i + 1][j];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j - 1];

                } else if (i == dim - 1 && j == 0) {
                    soma = matriz[i][j] * 3 + matriz[i - 1][j] * 2 + matriz[i - 1][j + 1] + matriz[i][j + 1] * 2;

                    for (int k = 0; k < 3; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i - 1][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j + 1];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j + 1];

                } else if (i == dim - 1 && j == dim - 1) {
                    soma = matriz[i][j] * 3 + matriz[i][j - 1] * 2 + matriz[i - 1][j - 1] + matriz[i - 1][j] * 2;

                    for (int k = 0; k < 3; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j - 1];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i - 1][j];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j - 1];

                } else if (i == 0) {
                    soma = matriz[i][j] * 2 + matriz[i - 1][j] * 2 + matriz[i - 1][j + 1] + matriz[i][j + 1] + matriz[i + 1][j] * 2 + matriz[i + 1][j + 1];

                    for (int k = 0; k < 2; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i - 1][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i + 1][j];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j + 1];
                    cont++;
                    pixels[cont] = matriz[i][j + 1];
                    cont++;
                    pixels[cont] = matriz[i + 1][j + 1];

                } else if (i == dim - 1) {
                    soma = matriz[i][j] * 2 + matriz[i - 1][j] + matriz[i - 1][j + 1] + matriz[i - 1][j - 1] + matriz[i][j - 1] * 2 + matriz[i][j + 1] * 2;

                    for (int k = 0; k < 2; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j - 1];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j + 1];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j - 1];
                    cont++;
                    pixels[cont] = matriz[i - 1][j + 1];
                    cont++;
                    pixels[cont] = matriz[i - 1][j - 1];

                } else if (i == 0) {
                    soma = matriz[i][j] * 2 + matriz[i][j - 1] * 2 + matriz[i + 1][j - 1] + matriz[i + 1][j] + matriz[i][j + 1] * 2 + matriz[i + 1][j + 1];

                    for (int k = 0; k < 2; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j - 1];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i][j + 1];
                        cont++;
                    }
                    pixels[cont] = matriz[i + 1][j - 1];
                    cont++;
                    pixels[cont] = matriz[i + 1][j];
                    cont++;
                    pixels[cont] = matriz[i + 1][j + 1];

                } else if (j == dim - 1) {
                    soma = matriz[i][j] * 2 + matriz[i - 1][j] * 2 + matriz[i + 1][j] * 2 + matriz[i - 1][j - 1] + matriz[i][j - 1] + matriz[i + 1][j - 1];

                    for (int k = 0; k < 2; k++) {
                        pixels[k] = matriz[i][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i - 1][j];
                        cont++;
                    }
                    for (int k = cont; k < cont + 2; k++) {
                        pixels[k] = matriz[i + 1][j];
                        cont++;
                    }
                    pixels[cont] = matriz[i - 1][j - 1];
                    cont++;
                    pixels[cont] = matriz[i][j - 1];
                    cont++;
                    pixels[cont] = matriz[i + 1][j - 1];

                } else {
                    soma = matriz[i][j] + matriz[i][j - 1] + matriz[i][j + 1] + matriz[i - 1][j] + matriz[i - 1][j - 1] + matriz[i - 1][j + 1] + matriz[i + 1][j] + matriz[i + 1][j + 1] + matriz[i + 1][j - 1];

                    pixels[0] = matriz[i][j];
                    pixels[1] = matriz[i][j - 1];
                    pixels[2] = matriz[i][j + 1];
                    pixels[3] = matriz[i - 1][j];
                    pixels[4] = matriz[i - 1][j - 1];
                    pixels[5] = matriz[i - 1][j + 1];
                    pixels[6] = matriz[i + 1][j];
                    pixels[7] = matriz[i + 1][j + 1];
                    pixels[8] = matriz[i + 1][j - 1];

                }
                cont = 0;
            }

        }
    }
}
