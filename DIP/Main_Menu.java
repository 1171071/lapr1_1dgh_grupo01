package DIP;

import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;
import org.la4j.Matrix;

public class Main_Menu {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException, ParseException, IOException, InterruptedException {
        boolean var = true;
        int op = 0, nEl = 0, dim = 0, cont1 = 0;
        File file = new File(args[0]);
        if (args.length > 0) {
            if (file.isDirectory()) {
                op = 1;
                String[] lista = file.list();
                for (int i = 0; i < lista.length; i++) {
                    System.out.println(lista[i]);
                    Runtime.getRuntime().exec("gnuplot -e " + "filename='" + lista[i] + "' script.gp");
                    System.out.println("Press ENTER to the next image");
                    in.nextLine();
                }
                System.out.println("Press ENTER to end the program");
                in.nextLine();

            } else {

                String fileName = args[0];
                String fileNameTemp = "New_Temp.txt";
                Scanner finput = new Scanner(new File(fileName));
                int[][] image = ReadTXTFile.loading(finput, nEl, dim);
                Matrix a = Utilities.convertToMatrix(image);
                nEl = image.length;
                int[][] nova = new int[nEl][nEl];
                for (int i = 0; i < nEl; i++) {
                    for (int j = 0; j < nEl; j++) {
                        nova[i][j] = image[i][j];
                    }
                }
                System.out.printf("%n%55s", "DIGITAL IMAGE PROCESSING\n");
                do {
                    System.out.printf("%n%46s", "Main Menu");
                    System.out.println("\n=======================================================================================\n");
                    System.out.println("Please choose an operation");
                    System.out.println(" 1 - Show Characteristics \n 2 - Apply Filters \n 3 - Apply Transformations \n 4 - Save Compressed Image \n 5 - Visualize Original Image \n 6 - Visualize Current Image \n 0 - Exit");
                    System.out.print("\nSelect an option: ");

                    op = in.nextInt();
                    in.nextLine();
                    switch (op) {
                        case 1:
                            Statistics.Statistics(image, nEl);
                            System.out.println();
                            break;
                        case 2:
                            nova = Filters.Options(image, nEl, nova, cont1, var);
                            cont1++;
                            break;
                        case 3:
                            nova = Transformations.transformations(a, nEl, nova, cont1, var);
                            cont1++;
                            break;
                        case 4:
                            Compress.compression(image, nEl, nova, cont1, var);
                            break;
                        case 5:
                            int op1 = 2;

                            Utilities.visualize(fileName, op1);
                            break;
                        case 6:
                            fileNameTemp = "New" + fileNameTemp + ".txt";
                            String res = "Y";
                            var = false;
                            Matrix mat = Utilities.convertToMatrix(nova);
                            nova = Utilities.saving(mat, res, image, fileNameTemp, var);
                            op1 = 1;
                            Utilities.visualize(fileNameTemp, op1);
                            var = true;
                            break;
                        case 0:
                            System.out.println();
                            break;
                        default:
                            break;

                    }

                } while (op != 0);
            }
        } else {
            System.out.println("\n\n\nINSERT YOUR ARGUMENT PLEASE\n\n\n");
        }
        System.out.println("\n\n\n\n\n\n\n\nCOPYRIGHT TEAM ESPERANTO \nBruno Escuta, Melanie Badura Ura, Pinex 90, Pedro Sousa, the King of the Cattle \nALL RIGHTS RESERVED");

    }
}
