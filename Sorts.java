package testes;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sorts {

    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        String fileName;
        int nEl = 0, dim = 0, op;
        int[][] image1;

        fileName = "testImage4.txt";
        System.out.println("\n\n\n======Matrix 4x4======\n");
        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        fileName = "testImage40.txt";
        System.out.println("\n\n\n======Matrix 40x40======\n");

        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        fileName = "testImage80.txt";
        System.out.println("\n\n\n======Matrix 80x80======\n");

        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        fileName = "testImage120.txt";
        System.out.println("\n\n\n======Matrix 120x120======\n");

        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        fileName = "testImage160.txt";
        System.out.println("\n\n\n======Matrix 160x160======\n");

        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        fileName = "testImage200.txt";
        System.out.println("\n\n\n======Matrix 200x200======\n");

        image1 = lerMatriz(nEl, dim, fileName);
        selectionSort(image1);
        bubbleSort(image1);
        insertionSort(image1);

        System.out.println("FIM DO PROGRAMA");

    }

    public static void selectionSort(int array[][]) {
        System.out.println("======SELECTION SORT======");
        long startTime = System.nanoTime();

        for (int g = 0; g < array.length; g++) {

            for (int i = 0; i < array.length - 1; i++) {
                int pos = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j][g] > array[pos][g]) {
                        pos = j;
                    }
                }
                if (pos != i) {
                    int x = array[i][g];
                    array[i][g] = array[pos][g];
                    array[pos][g] = x;
                }
            }

        }

        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

    }

    public static void bubbleSort(int[][] array) {
        System.out.println("======BUBBLE SORT======");
        long startTime = System.nanoTime();
        int aux;

        for (int g = 0; g < array.length; g++) {

            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length - 1; j++) {
                    if (array[j][g] < array[j + 1][g]) {
                        aux = array[j][g];
                        array[j][g] = array[j + 1][g];
                        array[j + 1][g] = aux;
                    }
                }
            }
        }
        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

    }

    public static void insertionSort(int[][] array) {
        int op2;
        System.out.println("======INSERTION SORT======");
        long startTime = System.nanoTime();
        int aux;
        for (int g = 0; g < array.length; g++) {
            for (int i = 1; i < array.length; i++) {

                aux = array[i][g];
                int j = i;
                while ((j > 0) && (array[j - 1][g] < aux)) {
                    array[j][g] = array[j - 1][g];
                    j -= 1;
                }
                array[j][g] = aux;
            }
        }
        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

    }

    public static int[][] lerMatriz(int nEl, int dim, String fileName) throws FileNotFoundException {
        Scanner finput = new Scanner(new File(fileName));

        int j = 0;
        String line = finput.nextLine();

        String dimensao = "";
        int i = (line.length() - 1);

        while (line.charAt(i) != ' ') {
            dimensao = line.charAt(i) + dimensao;
            i--;
        }

        dim = Integer.parseInt(dimensao);
        nEl = 2;
        int[][] image = new int[dim][dim];
        while (finput.hasNext() && nEl < dim + 2) {
            line = finput.nextLine();
            if (line.trim().length() > 0) {
                nEl = saveInMatrix(line, image, dim, nEl, j);
                j++;
            }
        }
        finput.close();
        return image;
    }

    public static int saveInMatrix(String line, int[][] image, int dim, int nEl, int j) {

        int num = 0;
        String[] strTemp = line.split(",");
        for (int i = 0; i < strTemp.length; i++) {
            String number = strTemp[i].trim();
            num = (int) Double.parseDouble(number);
            if (num < 0 || num > 255) {
                System.out.println(num + " - Número Incorreto");
                break;
            }
            image[j][i] = num;
        }

        nEl++;
        return nEl;
    }

}
