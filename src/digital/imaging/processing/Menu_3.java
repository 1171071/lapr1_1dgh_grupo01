package digital.imaging.processing;

import static digital.imaging.processing.Menu_2.nomeFich;
import static digital.imaging.processing.DIP.in;
import static digital.imaging.processing.Utilities.saving;
import java.io.FileNotFoundException;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Menu_3 {

    public static int[][] rotation(Matrix a, int nEl, int[][] nova, int cont1) throws FileNotFoundException {
        double[][] image2 = a.toDenseMatrix().toArray();
        int[][] image = new int[nEl][nEl];
        for (int p = 0; p < image2.length; p++) {
            for (int q = 0; q < image2.length; q++) {
                image[p][q] = (int) image2[p][q];
            }
        }
        int[][] rot = new int[nEl][nEl];
        double[][] rot2 = new double[nEl][nEl];
        Matrix mat = new Basic2DMatrix(rot2);
        char res = 0;
        if (cont1 != 0) {
            System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
            res = in.next().charAt(0);
            if (res == 'Y') {
                for (int i = 0; i < nEl; i++) {
                    for (int j = 0; j < nEl; j++) {
                        rot[i][j] = image[image.length - 1 - j][i];
                    }
                }
                Utilities.printMatrix(rot);
                nomeFich = nomeFich + "Rotate_90";
                nova = saving(mat, res, cont1, rot, nomeFich);
            }
            if (res == 'N') {
                for (int i = 0; i < nEl; i++) {
                    for (int j = 0; j < nEl; j++) {
                        rot[i][j] = nova[image.length - 1 - j][i];
                    }
                }
                Utilities.printMatrix(rot);
                nomeFich = nomeFich + "Rotate_90";
                nova = saving(mat, res, cont1, nova, nomeFich);
            }
        }
        if (cont1 == 0) {
            for (int i = 0; i < nEl; i++) {
                for (int j = 0; j < nEl; j++) {
                    rot[i][j] = image[image.length - 1 - j][i];
                }
            }
            Utilities.printMatrix(rot);
            nomeFich = nomeFich + "Rotate_90";
            nova = saving(mat, res, cont1, rot, nomeFich);
        }
        return nova;
    }
}
