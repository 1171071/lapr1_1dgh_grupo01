package digital.imaging.processing;

import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class DIP {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        int op = 0, nEl = 0, dim = 0, cont1 = 0;
        String fileName = args[0];
        Scanner finput = new Scanner(new File(fileName));
        int[][] image = loadInMatrix.loading(finput, nEl, dim);
        Matrix a = Utilities.convertToMatrix(image);
        nEl = image.length;
        int[][] nova = new int[nEl][nEl];
        double[][] temp = new double[nEl][nEl];
        Matrix n = Utilities.convertToMatrix(nova);
        do {
            System.out.println("Please choose an operation");
            System.out.println(" 1 - Shows Matricial Statistics \n 2 - Applies Filters \n 3 - Rotates Matrix Clockwise");
            System.out.println(" ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    Menu_1.Statistics(image, nEl);
                    System.out.println();
                    break;
                case 2:
                    nova = Menu_2.Options(image, nEl, nova, cont1);
                    cont1++;
                    break;
                case 3:
                    nova = Menu_3.rotation(a, nEl, nova, cont1);
                    cont1++;
                    break;
                default:
                    break;
            }

        } while (op < 4);
    }
}
