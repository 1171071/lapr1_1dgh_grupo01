package testes;

import java.util.Scanner;
import DIP.Statistics;
import static DIP.Statistics.medt;
import static DIP.Statistics.sumt;

public class testesEstatisticas {

    static Scanner ler = new Scanner(System.in);

    public static void main(int nEl, int[][] image) {

        System.out.println("What do you wish to test?");
        System.out.println("1- Lines");
        System.out.println("2- Columns");
        System.out.println("3- Matrix");
        System.out.println("0- Exit");
        int[] suml = new int[nEl];
        int[] sumc = new int[nEl];
        int[] medl = new int[nEl];
        int[] medc = new int[nEl];
        int somat = 0;
        int mediat = 0;
        int esperada = 0;
        System.out.print("\nSelect an option: ");
        int esc = ler.nextInt();
        suml = Statistics.suml(image);
        sumc = Statistics.sumc(image);
        medl = Statistics.medl(image, suml);
        medc = Statistics.medc(image, sumc);
        somat = sumt(suml, sumc, nEl);
        mediat = medt(somat, nEl);

        if (esc == 1) {
            System.out.printf("%n%14s", "Testing Menu - Lines");
            System.out.println("\n=============================");
            System.out.println("Do you wish to test Sum or Average:");
            System.out.println("1- Sum");
            System.out.println("2- Average");
            System.out.print("\nSelect an option: ");
            int esc2 = ler.nextInt();
            if (esc2 == 1) {

                testeSumLinhas(nEl, suml, image, esperada);
                esperada = 0;
            } else if (esc2 == 2) {

                testeAverageLinhas(nEl, medl, image, esperada);
                esperada = 0;
                medl = new int[nEl];
            }
        } else if (esc == 2) {
            System.out.printf("%n%14s", "Testing Menu - Columns");
            System.out.println("\n=============================");
            System.out.println("Test sum or average:");
            System.out.println("1- Sum");
            System.out.println("2- Average");
            System.out.print("\nSelect an option: ");
            int esc2 = ler.nextInt();
            if (esc2 == 1) {
                testeSumColunas(nEl, sumc, image, esperada);
                esperada = 0;
                sumc = new int[nEl];
            } else if (esc2 == 2) {
                testeAverageColunas(nEl, medc, image, esperada);
                esperada = 0;
                medc = new int[nEl];
            }
        } else if (esc == 3) {
            System.out.printf("%n%14s", "Testing Menu - Matrix");
            System.out.println("\n=============================");
            System.out.println("Test sum or average:");
            System.out.println("1- Sum");
            System.out.println("2- Average");
            System.out.print("\nSelect an option: ");
            int esc2 = ler.nextInt();
            if (esc2 == 1) {
                testeSumGeral(nEl, somat, esperada);
                esperada = 0;
                somat = 0;
            } else if (esc2 == 2) {
                testeAverageGeral(nEl, mediat, esperada);
                esperada = 0;
                mediat = 0;
            }
        }

    }

    public static void testeSumLinhas(int nEl, int[] suml, int image[][], int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Sum");
        System.out.println("\n=============================");
        for (int i = 0; i < nEl; i++) {
            System.out.print("Expected Result of Line " + (i + 1) + " :");
            esperada = ler.nextInt();
            for (int cont = 0; cont < image.length; cont++) {
                if (esperada == suml[i]) {
                    System.out.println("Sum of Line " + (i + 1) + " is CORRECT");
                    break;
                } else {
                    System.out.println("Sum of Line " + (i + 1) + " is INCORRECT");
                    break;
                }
            }
        }

    }

    public static void testeSumColunas(int nEl, int[] sumc, int image[][], int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Sum");
        System.out.println("\n=============================");
        for (int i = 0; i < nEl; i++) {
            System.out.print("Expected Result of Column " + (i + 1) + " :");
            esperada = ler.nextInt();
            for (int cont = 0; cont < image.length; cont++) {
                if (esperada == sumc[i]) {
                    System.out.println("Sum of Column " + (i + 1) + " is CORRECT");
                    break;
                } else {
                    System.out.println("Sum of Column " + (i + 1) + " is INCORRECT");
                    break;
                }
            }
        }
    }

    public static void testeAverageLinhas(int nEl, int[] medl, int image[][], int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Average");
        System.out.println("\n=============================");
        for (int i = 0; i < nEl; i++) {
            System.out.print("Expected Result of Line " + (i + 1) + " :");
            esperada = ler.nextInt();
            for (int cont = 0; cont < image.length; cont++) {
                if (esperada == medl[i]) {
                    System.out.println("Average of Line " + (i + 1) + " is CORRECT");
                    break;
                } else {
                    System.out.println("Average of Line " + (i + 1) + " is INCORRECT");
                    break;
                }
            }
        }
    }

    public static void testeAverageColunas(int nEl, int[] medc, int image[][], int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Average");
        System.out.println("\n=============================");
        for (int i = 0; i < nEl; i++) {
            System.out.print("Expected Result of Column " + (i + 1) + " :");
            esperada = ler.nextInt();
            for (int cont = 0; cont < image.length; cont++) {
                if (esperada == medc[i]) {
                    System.out.println("Average of Column " + (i + 1) + " is CORRECT");
                    break;
                } else {
                    System.out.println("Average of Column " + (i + i) + " is INCORRECT");
                    break;
                }
            }
        }
    }

    public static void testeSumGeral(int nEl, int somat, int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Sum");
        System.out.println("\n=============================");
        System.out.print("Expected Result of the Matrix: ");
        esperada = ler.nextInt();
        if (esperada == somat) {
            System.out.println("The Sum is CORRECT");
        } else {
            System.out.println("The Sum is INCORRECT");
        }
    }

    public static void testeAverageGeral(int nEl, int mediat, int esperada) {
        System.out.printf("%n%14s", "Testing Menu - Average");
        System.out.println("\n=============================");
        System.out.print("Expected Result of the Matrix: ");
        esperada = ler.nextInt();
        if (esperada == mediat) {
            System.out.println("The Average is CORRECT");
        } else {
            System.out.println("The Average is INCORRECT");
        }
    }
}
