package testes;

import static DIP.Main_Menu.in;
import DIP.Utilities;
import org.la4j.Matrix;
import org.la4j.decomposition.SingularValueDecompositor;

public class testesCompressão {

    public static void testeComp(int[][] image, int nEl) {

        System.out.println("What is the compression quality? (Insert value from 1 - " + nEl + ")");
        int op = in.nextInt();
        Matrix temp = Utilities.convertToMatrix(image);
        SingularValueDecompositor eigenD;
        eigenD = new SingularValueDecompositor(temp);
        Matrix[] mattD = eigenD.decompose();

        double matU[][] = mattD[0].toDenseMatrix().toArray();
        double matD[][] = mattD[1].toDenseMatrix().toArray();
        double matV[][] = mattD[2].toDenseMatrix().toArray();

        double[][] rec1 = Utilities.reconstruction(nEl, op, matU, matV, matD);
        int[][] rec = new int[nEl][nEl];

        for (int i = 0; i < nEl; i++) {
            for (int j = 0; j < nEl; j++) {
                rec[i][j] = (int) rec1[i][j];
            }
        }
        testesFiltros.filter(rec, image, nEl);
    }
}
