package testes;

import static DIP.Main_Menu.in;
import DIP.Utilities;
import java.io.FileNotFoundException;
import static testes.testesEstatisticas.ler;

public class testesFiltros {

    public static void mainFilter(int[][] image, int nEl) throws FileNotFoundException {
        System.out.println("What filter do you wish to test?");
        System.out.println("1- Smoothing (Average)");
        System.out.println("2- Reduce Noise (Median)");
        System.out.println("3- Intensify Regions (Maximum)");
        System.out.println("4- Intensify Regions(Minimum)");
        System.out.println("0- Exit");
        System.out.print("\nSelect an option: ");
        int esc = ler.nextInt();

        if (esc == 1) {
            System.out.printf("%n%14s", "Testing Menu - Smoothing");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.average(aux, nEl);
            filter(matrix, image, nEl);
        } else if (esc == 2) {
            System.out.printf("%n%14s", "Testing Menu - Noise Reduction");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.mediana(aux, nEl);
            filter(matrix, image, nEl);
        } else if (esc == 3) {
            System.out.printf("%n%14s", "Testing Menu - Intensify Bright Regions");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.maxnmin(aux, nEl, esc);
            filter(matrix, image, nEl);
        } else if (esc == 4) {
            System.out.printf("%n%14s", "Testing Menu - Intensify Dark Regions");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.maxnmin(aux, nEl, esc);
            filter(matrix, image, nEl);
        } else if (esc == 5) {
            System.out.printf("%n%14s", "Testing Menu - Detect Borders");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.var(aux, nEl);
            filter(matrix, image, nEl);
        } else if (esc == 6) {
            System.out.printf("%n%14s", "Testing Menu - Mask");
            System.out.println("\n=============================");
            int[][] aux = DIP.Utilities.expandMatrix(image, nEl);
            int[][] matrix = DIP.Filters.comb(aux, nEl);
            filter(matrix, image, nEl);
        } else {
            System.out.println("Wrong option");
        }

    }

    public static int[][] fetchMatrix(int[][] image, int nEl) {
        int num = 0;
        int[][] expected = new int[nEl][nEl];
        System.out.println("Please insert expected matrix values");
        for (int i = 0; i < image.length; i++) {
            System.out.println("Line " + (i + 1));
            for (int j = 0; j < image.length; j++) {
                num = in.nextInt();
                expected[i][j] = num;
            }
        }
        return expected;
    }

    public static void filter(int[][] matrix, int[][] image, int nEl) {
        int[][] expected = fetchMatrix(image, nEl);
        boolean ver = true;
        System.out.println("\nExpected Matrix");
        Utilities.printMatrix(expected);
        System.out.println("\nCalculated Matrix");
        Utilities.printMatrix(matrix);
        System.out.println();
        for (int x = 0;
                x < image.length;
                x++) {
            for (int y = 0; y < image.length; y++) {
                if (expected[x][y] != matrix[x][y]) {
                    System.out.println("Line " + (x + 1) + " is INCORRECT");
                    ver = false;
                    break;
                }
            }
        }
        if (ver == true) {
            System.out.println("Matrix is CORRECT");
        }
    }
}
