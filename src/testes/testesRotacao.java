package testes;

import DIP.Transformations;
import DIP.Utilities;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.la4j.Matrix;

public class testesRotacao {

    public static void rodado(int image[][], int nEl, int[][] rotacao) throws FileNotFoundException, IOException {
        boolean var = false;
        String fileName = "_";
        Matrix a = Utilities.convertToMatrix(image);
        int cont1 = 0,op = 0;
        System.out.println("\nExpected Matrix");
        rotacao = Transformations.rotation(a, nEl, rotacao, cont1, var,op, fileName);
        System.out.println("\nCalculated Matrix");
        Utilities.printMatrix(rotacao);
        for (int i = 0; i < nEl; i++) {
            boolean rod = true;
            for (int j = 0; j < nEl; j++) {
                if (image[i][j] != rotacao[j][nEl - 1 - i]) {
                    rod = false;
                }
            }
            if (rod == true) {
                System.out.println("\nLine " + (i + 1) + " Becomes Column " + (nEl - 1 - i) + " - CORRECT");
            } else {
                System.out.println("Linha " + (i + 1) + " Becomes Column " + (nEl - 1 - i) + " - INCORRECT");
            }
        }

    }
}
