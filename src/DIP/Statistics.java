package DIP;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;


public class Statistics {

    public static int somal = 0, somac = 0, medial = 0, mediac = 0;

    public static void Statistics(int[][] image, int nEl) {
        System.out.printf("%n%46s", "Statistics");
        System.out.println("\n=======================================================================================\n");
        int[] suml = suml(image);
        int[] medial = medl(image, suml);
        System.out.println("=========== LINES ===========");
        for (int i = 0; i < image.length; i++) {
            System.out.println("Line: " + (i + 1));
            System.out.println("Sum= " + suml[i]);
            System.out.println("Average= " + medial[i] + "\n");
        }

        int[] sumc = sumc(image);
        int[] mediac = medc(image, suml);
        System.out.println("=========== COLUMNS ===========");
        for (int i = 0; i < image.length; i++) {
            System.out.println("COLUMN: " + (i + 1));
            System.out.println("Sum= " + sumc[i]);
            System.out.println("Average= " + mediac[i] + "\n    ");
        }

        int somat = sumt(suml, sumc, nEl);
        int mediat = medt(somat, nEl);
        System.out.println(
                "=========== MATRIX ===========");
        System.out.println(
                "The Sum of All the Elements is " + somat);
        System.out.println(
                "The Average of All the Elements is " + mediat + "\n");

        Matrix temp = Utilities.convertToMatrix(image);

        EigenDecompositor eigenD = new EigenDecompositor(temp);

        Matrix[] mattD = eigenD.decompose();

        System.out.println(
                "=========== EIGENVECTORS ===========");
        System.out.println(
                "Each column represents a single eigenvector \n");
        System.out.println(mattD[0]);
        System.out.println(
                "=========== EIGENVALUES ===========");
        System.out.println(
                "Each number in the diagonal represents an eigenvalue. If it has a number \nin same line, it is an imaginary number that is added to the eigenvalue \n");
        System.out.println(mattD[1]);
        System.out.println(
                "=========== Trapezoidal Rule ===========");
        integrate(image);
    }

    public static int[] suml(int[][] image) {
        somal = 0;
        int[] suml = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                somal += image[i][j];
            }
            suml[cont] = (int) somal;
            cont++;
            somal = 0;
        }

        return suml;
    }

    public static int[] sumc(int[][] image) {
        somac = 0;
        int[] sumc = new int[image.length];
        int cont = 0;

        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                somac += image[j][i];
            }
            sumc[cont] = (int) somac;
            cont++;
            somac = 0;
        }
        return sumc;
    }

    public static int sumt(int[] suml, int[] sumc, int nEl) {
        int sumt = 0;
        for (int j = 0; j < nEl; j++) {
            sumt += suml[j];
        }
        return sumt;
    }

    public static int[] medl(int[][] image, int[] suml) {
        int[] medl = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            somal = suml[cont];
            medial = (somal / image.length);
            medl[i] = medial;
            medial = 0;
            cont++;

        }
        return medl;
    }

    public static int[] medc(int[][] image, int[] sumc) {
        int[] medc = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            somac = sumc[cont];
            mediac = (somac / image.length);
            medc[i] = mediac;
            mediac = 0;
            cont++;

        }
        return medc;
    }

    public static int medt(int sumt, int nEl) {
        int mediat = 0;
        mediat = (sumt / (nEl * nEl));
        return mediat;
    }

    public static void integrate(int[][] matrix) {
        int result, sum = 0, a = 1, b = matrix.length;

        for (int i = 0; i < matrix.length; i++) {
            sum=0;
            for (int j = 0; j < matrix.length; j++) {
                sum += matrix[i][j];

                if (j != 0 || j != matrix.length) {
                    sum += matrix[i][j];

                }
            }

            result = ((b - a) / 2) * sum;
            System.out.println("Statistic of the trapizoidal rule of line "+(i+1)+" is "+result);
        }
    }

}
