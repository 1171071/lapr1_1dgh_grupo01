/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DIP;

/**
 *
 * @author Carlos Ferreira
 */
//exemplo de utilização desta biblioteca

import org.la4j.Matrix;


import org.la4j.matrix.dense.Basic2DMatrix;

import org.la4j.decomposition.SingularValueDecompositor;



/**

 *

 * @author cferreira

 */

public class ExemploIntegracao_la4j{



    /**

     * @param args the command line arguments

     */

    public static void main(String[] args) {

       

        

        //double matComparacao[][]={{0.8, 0.3},{0.2,0.7}};
        double matComparacao[][]={{1,1,1,1,1,1}, {1,1,1,1,1,1},{1,40,80,100,255,255}, {1,40,80,100,255,255}, {1,1,1,1,1,1},{1,1,1,1,1,1}};
        
		// Criar objeto do tipo Matriz
        int size=matComparacao.length;
        Matrix a = new Basic2DMatrix(matComparacao); 

        

        // SVD decomposition
        SingularValueDecompositor eigenD = new SingularValueDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

            
        System.out.println(a);
        for(int i=0; i<3;i++)

        {

            System.out.println(mattD[i]);

        }

        

        // converte objeto Matrix (duas matrizes)  para array Java        

        double matU [][]= mattD[0].toDenseMatrix().toArray();

        double matD [][]= mattD[1].toDenseMatrix().toArray();

        double matV [][]= mattD[2].toDenseMatrix().toArray();

    
    
        //Print column de matriz U
    for( int i=0; i<6; i++)
    {
      System.out.println(matU[i][0]);  
    }
    
    System.out.println("\n");
    System.out.println(matD[0][0]+"\n");
    
    //Print column de matriz V
    for( int i=0; i<6; i++)
    {
      System.out.println(matV[i][0]);  
    }
    
    
    
    
    
}

    
}