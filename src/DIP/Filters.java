package DIP;

import static DIP.Main_Menu.in;
import static DIP.Utilities.expandMatrix;
import static DIP.Utilities.storing;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Filters {

    static String nomeFich = "_";

    public static int[][] Options(int[][] image, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException, IOException {
        int op = 0, cont = 0;

        String fileName = "_";
        char res = 0;
        int[][] aux = expandMatrix(image, nEl);
        int[][] matrix = new int[nEl][nEl];
        Matrix mat = Utilities.convertToMatrix(matrix);

        do {
            System.out.printf("%n%46s", "Filters");
            System.out.println("\n=======================================================================================\n");
            System.out.println("What filter do you wish to apply?");
            System.out.println(" 1 - Smoothing (Average) \n 2 - Noise Reduction (Median) \n 3 - Intensify Bright Regions (Maximum) \n 4 - Intensify Dark Regions (Minimum) \n 5 - Detect Existing Borders (Variance) \n 6 - Mask (Convulation) \n 0 - Exit");
            System.out.print("\nSelect an option: ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = average(aux, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = average(nova, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = average(aux, nEl);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 2:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = mediana(aux, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = mediana(nova, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = mediana(aux, nEl);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 3:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = maxnmin(aux, nEl, op);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = maxnmin(nova, nEl, op);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = maxnmin(aux, nEl, op);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 4:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = maxnmin(aux, nEl, op);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = maxnmin(nova, nEl, op);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = maxnmin(aux, nEl, op);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 5:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = var(aux, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = comb(nova, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = var(aux, nEl);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 6:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = comb(aux, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = comb(nova, nEl);
                            nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                            break;
                        }
                    }
                    matrix = comb(aux, nEl);
                    nova = Utilities.printFilterInfo(image, matrix, mat, nova, res, var, op, fileName);
                    break;
                case 0:
                    System.out.println();
                    break;
                default:
                    break;
            }
            cont++;
        } while (op != 0);
        return nova;
    }

    public static int[][] average(int[][] aux, int nEl) {
        int sum = 0;
        int avg = 0;
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b;
        int[][] average = new int[nEl][nEl];
        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux[i].length - 1; j++) {
                a = i;
                b = j;
                do {
                    x = i - 1;
                    do {
                        y = j - 1;
                        sum = sum + aux[x][y];
                        cont++;
                        j++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    i++;
                    j = b;
                } while (cont1 < 3);
                cont1 = 0;
                i = a;
                j = b;

                avg = sum / 9;
                average[i - 1][j - 1] = avg;
                sum = 0;
                avg = 0;
            }

        }
        return average;
    }

    public static int[][] mediana(int[][] aux, int nEl) {
        int[] med = new int[9];
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b, q = 0;
        int[][] mediana = new int[nEl][nEl];
        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux[i].length - 1; j++) {
                a = i;
                b = j;
                do {
                    x = i - 1;
                    do {
                        y = j - 1;
                        med[q] = aux[x][y];
                        q++;
                        cont++;
                        j++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    i++;
                    j = b;

                } while (cont1 < 3);
                cont1 = 0;
                i = a;
                j = b;
                for (int l = 0; l < 8; l++) {
                    for (int p = 0; p < 8; p++) {
                        if (med[p] > med[p + 1]) {
                            double t = med[p];
                            med[p] = med[p + 1];
                            med[p + 1] = (int) t;
                        }
                    }
                }
                mediana[i - 1][j - 1] = (int) med[4];
                q = 0;
            }
        }
        return mediana;
    }

    public static int[][] maxnmin(int[][] aux, int nEl, int op) {
        int[] arr = new int[9];
        double[][] array = new double[nEl][nEl];
        int[][] maxmin = new int[nEl][nEl];
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b, q = 0;
        Matrix mat = new Basic2DMatrix(array);

        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux.length - 1; j++) {
                a = i;
                b = j;
                x = i - 1;
                do {

                    y = j - 1;
                    do {
                        arr[q] = aux[x][y];
                        q++;
                        cont++;
                        y++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    x++;
                    y = b;
                } while (cont1 < 3);
                cont1 = 0;
                x = a;
                y = b;
                for (int l = 0; l < 8; l++) {
                    for (int p = 0; p < 8; p++) {
                        if (arr[p] > arr[p + 1]) {
                            double t = arr[p];
                            arr[p] = arr[p + 1];
                            arr[p + 1] = (int) t;
                        }
                    }
                }
                mat = storing(op, array, arr, nEl, i, j, mat);
                double[][] maxminD = mat.toDenseMatrix().toArray();
                for (int t = 0; t < maxminD.length; t++) {
                    for (int u = 0; u < maxminD.length; u++) {
                        maxmin[t][u] = (int) maxminD[t][u];
                    }
                }
                q = 0;
            }
        }
        return maxmin;
    }

    public static int[][] var(int[][] aux, int nEl) {
        int[][] matrix = new int[nEl][nEl];
        int a = 0, b = 0, x = 0, y = 0, cont = 0, cont1 = 0, sum = 0;
        double var = 0;
        int[][] aux2 = average(aux, nEl);
        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux[i].length - 1; j++) {
                sum = 0;
                int k = aux2[i - 1][j - 1];
                a = i;
                b = j;
                do {
                    x = i - 1;
                    do {
                        y = j - 1;
                        sum = sum + (k - aux[x][y]) * (k - aux[x][y]);
                        cont++;
                        j++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    i++;
                    j = b;
                } while (cont1 < 3);
                cont1 = 0;
                i = a;
                j = b;
                var = sum / 9;
                if (var > 255) {
                    var = 255;
                }
                if (var < 0) {
                    var = 0;
                }
                matrix[i - 1][j - 1] = (int) var;
            }
        }
        return matrix;
    }

    public static int[][] comb(int[][] aux, int nEl) throws FileNotFoundException {
        int cont1 = 0, cont2 = 0;
        int[][] matrix = new int[nEl][nEl];
        double[][] mask = new double[3][3];
        mask = readMask(mask);
        mask = normMask(mask);
        for (int i = 1; i < (aux.length - 1); i++) {
            for (int j = 1; j < (aux.length - 1); j++) {
                matrix[cont1][cont2] = combulation(aux, i, j, mask);
                cont2++;
            }
            cont2 = 0;
            cont1++;
        }
        return matrix;
    }

    public static double[][] readMask(double[][] mask) {
        double a;
        System.out.println("Mask");
        System.out.println("================================");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("Enter the value of the mask in the (" + (i + 1) + "," + (j + 1) + ") postition.");
                a = in.nextDouble();
                in.nextLine();
                mask[i][j] = a;
            }
        }

        return mask;
    }

    public static double[][] normMask(double[][] mask) {
        int soma = 0;
        double pixel;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                soma += mask[i][j];
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                pixel = mask[i][j] / soma;
                mask[i][j] = pixel;
            }
        }
        return mask;
    }

    public static int combulation(int[][] matrixAum, int i, int j, double[][] mask) {
        int niceSum, cont1 = 0, cont2 = 0;
        double sum = 0;
        for (int c = i - 1; c < i + 2; c++) {
            for (int k = j - 1; k < j + 2; k++) {
                sum += (matrixAum[c][k] * mask[cont1][cont2]);
                cont2++;
            }
            cont2 = 0;
            cont1++;
        }
        niceSum = arredondamentos(sum);
        return niceSum;
    }

    public static int arredondamentos(double pixel) {
        int pixelFinal;
        double resto;
        resto = pixel % 1;
        if (resto >= 0.5 && pixel < 255) {
            pixel++;
            pixel = pixel - resto;
            pixelFinal = (int) (pixel);
        } else {
            pixelFinal = (int) (pixel);
        }
        return pixelFinal;
    }
}
