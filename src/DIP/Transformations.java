package DIP;

import static DIP.Main_Menu.in;
import static DIP.Utilities.saving;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.la4j.Matrix;

public class Transformations {

    public static int[][] transformations(Matrix a, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException, IOException {
        int op = 0;
        String fileName = "_";
        do {
            System.out.printf("%n%55s", "Transformations");
            System.out.println("\n=======================================================================================\n");
            System.out.println("What transformation do you wish to apply?");
            System.out.println(" 1 - Rotate 90º Clockwise \n 2 - Sort Each Column \n 0 - Exit");
            System.out.print("\nSelect an option: ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    System.out.println("Do you wish to rotate it clockwise (1) or anticlockwise (2)?");
                    op = in.nextInt();
                    in.nextLine();
                    System.out.printf("%n%44s", "Rotation");
                    System.out.println("\n=======================================================================================\n");
                    nova = rotation(a, nEl, nova, cont1, var, op, fileName);
                    break;
                case 2:
                    System.out.printf("%n%44s", "Sort");
                    System.out.println("\n=======================================================================================\n");
                    nova = sort(a, nEl, nova, cont1, var, fileName);
                    break;
                case 0:
                    System.out.println();
                    break;
                default:
                    break;
            }
            cont1++;
        } while (op != 0);
        return nova;
    }

    public static int[][] rotation(Matrix a, int nEl, int[][] nova, int cont1, boolean var, int op, String fileName) throws FileNotFoundException, IOException {
        double[][] image2 = a.toDenseMatrix().toArray();
        int[][] image = new int[nEl][nEl];
        for (int p = 0; p < image2.length; p++) {
            for (int q = 0; q < image2.length; q++) {
                image[p][q] = (int) image2[p][q];
            }
        }
        int[][] rot = new int[nEl][nEl];
        Matrix mat = Utilities.convertToMatrix(rot);
        char res = 0;
        if (cont1 != 0 && op != 3) {
            System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
            res = in.next().charAt(0);
            if (res == 'Y') {
                rot = Utilities.rotate(nEl, image, op);
                op = 7;
                nova = Utilities.printFilterInfo(image, rot, mat, nova, res, var, op, fileName);
            }
            if (res == 'N') {

                rot = Utilities.rotate(nEl, nova, op);
                op = 7;
                nova = Utilities.printFilterInfo(image, rot, mat, nova, res, var, op, fileName);
            }
        }
        if (cont1 == 0) {

            rot = Utilities.rotate(nEl, image, op);
            op = 7;
            nova = Utilities.printFilterInfo(image, rot, mat, nova, res, var, op, fileName);
        }
        if (op == 3) {

            nova = Utilities.rotate(nEl, nova, op);
        }
        return nova;
    }

    public static int[][] sort(Matrix a, int nEl, int[][] nova, int cont1, boolean var, String fileName) throws FileNotFoundException, IOException {
        int aux = 0, op = 8;
        char res = 0;
        double[][] image2 = a.toDenseMatrix().toArray();
        int[][] image = new int[nEl][nEl];
        int[][] sorted = new int[nEl][nEl];
        for (int p = 0; p < image2.length; p++) {
            for (int q = 0; q < image2.length; q++) {
                image[p][q] = (int) image2[p][q];

            }
        }

        Matrix mat = Utilities.convertToMatrix(sorted);
        if (cont1 != 0) {
            System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
            res = in.next().charAt(0);
            if (res == 'Y') {
                for (int p = 0; p < image2.length; p++) {
                    for (int q = 0; q < image2.length; q++) {
                        sorted[p][q] = image[p][q];
                    }
                }
                for (int p = 0; p < image.length; p++) {
                    for (int g = 0; g < image.length; g++) {
                        for (int i = 0; i < image.length - 1; i++) {
                            if (sorted[i][g] < sorted[i + 1][g]) {
                                aux = sorted[i][g];
                                sorted[i][g] = sorted[i + 1][g];
                                sorted[i + 1][g] = aux;
                            }
                        }
                    }
                }
                mat = Utilities.convertToMatrix(sorted);
                nova = Utilities.printFilterInfo(image, sorted, mat, nova, res, var, op, fileName);
            }

            if (res == 'N') {
                for (int p = 0; p < image2.length; p++) {
                    for (int q = 0; q < image2.length; q++) {
                        sorted[p][q] = nova[p][q];
                    }
                }
                for (int p = 0; p < image.length; p++) {
                    for (int g = 0; g < image.length; g++) {
                        for (int i = 0; i < image.length - 1; i++) {
                            if (sorted[i][g] < sorted[i + 1][g]) {
                                aux = sorted[i][g];
                                sorted[i][g] = sorted[i + 1][g];
                                sorted[i + 1][g] = aux;
                            }
                        }
                    }
                }
                mat = Utilities.convertToMatrix(sorted);
                nova = Utilities.printFilterInfo(image, sorted, mat, nova, res, var, op, fileName);
            }
        }
        if (cont1 == 0) {
            for (int p = 0; p < image2.length; p++) {
                for (int q = 0; q < image2.length; q++) {
                    sorted[p][q] = image[p][q];
                }
            }
            for (int p = 0; p < image.length; p++) {
                for (int g = 0; g < image.length; g++) {
                    for (int i = 0; i < image.length - 1; i++) {
                        if (sorted[i][g] < sorted[i + 1][g]) {
                            aux = sorted[i][g];
                            sorted[i][g] = sorted[i + 1][g];
                            sorted[i + 1][g] = aux;
                        }
                    }
                }
            }
            mat = Utilities.convertToMatrix(sorted);
            nova = Utilities.printFilterInfo(image, sorted, mat, nova, res, var, op, fileName);
        }
        return nova;
    }
}
