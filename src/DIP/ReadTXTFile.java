    package DIP;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Scanner;

public class ReadTXTFile {

    public static int[][] loading(Scanner finput, int nEl, int dim) throws ParseException {
        int j = 0, z = 0;
        String line = finput.nextLine();
        String[] temp1 = line.split(" ");
        String dimensao = temp1[2];
        dim = (int) Double.parseDouble(dimensao);
        int[][] image = new int[dim][dim];
        line = finput.nextLine();
        while (finput.hasNext() && nEl < dim) {
            line = finput.nextLine();
            String[] strTemp = line.split(",");
            if (strTemp.length == 1) {
                image = saveCompressed(dim, z, finput, strTemp, line, image);
                z++;
            } else {
                nEl = saveInMatrix(finput, strTemp, image, dim, nEl, j);
                j++;
            }
        }
        finput.close();
        return image;
    }

    public static int saveInMatrix(Scanner finput, String[] strTemp, int[][] image, int dim, int nEl, int j) {
        int num = 0;
        for (int i = 0; i < strTemp.length; i++) {
            String number = strTemp[i].trim();
            num = Integer.parseInt(number);
            if (num < 0 || num > 255) {
                System.out.println(num + " - Número Incorreto");
                break;
            }
            image[j][i] = num;
        }

        nEl++;
        return nEl;

    }

    public static int[][] saveCompressed(int dim, int z, Scanner finput, String[] strTemp, String line, int[][] image) throws ParseException {
        String[] temp = new String[dim];
        String[][] matD = new String[dim][dim];
        String[][] matU = new String[dim][dim];
        String[][] matV = new String[dim][dim];
        DecimalFormat df = new DecimalFormat();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        df.setDecimalFormatSymbols(symbols);
        matD[z][z] = strTemp[0];
        line = finput.nextLine();
        temp = line.split(",");
        for (int y = 0; y < dim; y++) {

            matU[y][z] = temp[y];
        }
        line = finput.nextLine();
        temp = line.split(",");
        for (int x = 0; x < dim; x++) {
            matV[x][z] = temp[x];
        }
        for (int y = 0; y < dim; y++) {
            String vstr = matV[y][z];
            double v = Double.valueOf(vstr);
            for (int x = 0; x < dim; x++) {
                String diagonal = matD[z][z];
                String ustr = matU[x][z];
                double d = Double.valueOf(diagonal);
                double u = Double.valueOf(ustr);
                double sum = u * d * v;
                if (sum > 255) {
                    sum = 255;
                }
                if (sum < 0) {
                    sum = 0;
                }
                image[x][y] += sum;
            }
        }
        return image;
    }
}
