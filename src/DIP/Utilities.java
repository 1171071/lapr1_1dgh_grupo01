package DIP;

import static DIP.Main_Menu.in;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Locale;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Utilities {

    public static Runtime rt = Runtime.getRuntime();

    public static int[][] expandMatrix(int[][] image, int nEl) {

        int[][] aux = new int[image.length + 2][image[0].length + 2];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                aux[i + 1][j + 1] = image[i][j];
            }
        }
        for (int j = 0; j < image[0].length; j++) {
            aux[0][j + 1] = image[0][j];
            aux[aux.length - 1][j + 1] = image[image.length - 1][j];
        }
        for (int i = 0; i < aux.length; i++) {
            aux[i][0] = aux[i][1];
            aux[i][aux[i].length - 1] = aux[i][aux[i].length - 2];
        }

        return aux;
    }

    public static Matrix storing(int op, double[][] array, int[] arr, int nEl, int i, int j, Matrix mat) {
        if (op == 3) {
            array[i - 1][j - 1] = arr[8];
        } else if (op == 4) {
            array[i - 1][j - 1] = arr[0];
        }

        return mat;
    }

    public static int[][] saving(Matrix mat, char res, int[][] image, String nomeFich, boolean var) throws FileNotFoundException {
        double novva[][] = mat.toDenseMatrix().toArray();
        int cont = 0;
        int[][] nova = new int[image.length][image.length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                nova[i][j] = (int) novva[i][j];
            }
        }
        if (nomeFich != "_Temp") {
            if (var == true) {
                System.out.println("\nDo you wish to save the changes? (Y/N)");
                res = in.next().charAt(0);
                System.out.println();
                if (res == 'Y') {
                    try (PrintWriter out = new PrintWriter(new File("New" + nomeFich + ".txt"))) {
                        out.format("#Grey Square " + image.length);
                        out.format("%n");
                        out.format("%n");
                        for (int t = 0; t < image.length; t++) {
                            for (int u = 0; u < image.length; u++) {
                                if (u == image.length - 1) {
                                    out.print(nova[t][u]);
                                    out.format("%n");
                                } else {
                                    out.print(nova[t][u] + ",");
                                }
                            }
                        }
                    }
                }
            }
        } else {
            try (PrintWriter out = new PrintWriter(new File("New" + nomeFich + ".txt"))) {
                out.format("#Grey Square " + image.length);
                out.format("%n");
                out.format("%n");
                for (int t = 0; t < image.length; t++) {
                    for (int u = 0; u < image.length; u++) {
                        if (u == image.length - 1) {
                            out.print(nova[t][u]);
                            out.format("%n");
                        } else {
                            out.print(nova[t][u] + ",");
                        }
                    }
                }
            }

        }
        return nova;
    }

    public static Matrix convertToMatrix(int[][] image) {
        double[][] img = new double[image.length][image.length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                img[i][j] = image[i][j];
            }
        }
        Matrix temp = new Basic2DMatrix(img);
        return temp;
    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] rotate(int nEl, int[][] image, int op) {
        int[][] rot = new int[nEl][nEl];
        if (op == 1) {
            for (int i = 0; i < nEl; i++) {
                for (int j = 0; j < nEl; j++) {
                    rot[i][j] = image[image.length - 1 - j][i];
                }
            }
        } else if (op == 2) {
            for (int i = 0; i < nEl; i++) {
                for (int j = 0; j < nEl; j++) {
                    rot[i][j] = image[j][image.length - 1 - i];
                }
            }
        }
        return rot;
    }

    public static void saveCompressed(int op, int nEl, double[][] matU, double[][] matD, double[][] matV) throws FileNotFoundException {
        Locale locale = Locale.UK;
        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        try (PrintWriter output = new PrintWriter(new File("MatrixC" + op + ".txt"))) {
            output.format("#Grey Square " + nEl + "\n\n");
            for (int z = 0; z < op; z++) {
                output.println(nf.format(matD[z][z]).replace(",", "") + " ");
                for (int y = 0; y < nEl; y++) {
                    output.print(nf.format(matU[y][z]) + ",");
                }
                output.println();
                for (int x = 0; x < nEl; x++) {
                    output.print(nf.format(matV[x][z]) + ",");
                }
                output.println();
            }
        }
    }

    public static double eam(int nEl, int[][] image, int op, double matU[][], double matV[][], double matD[][]) {
        double eam = 0, valorR, valor, sum = 0, div, sumt = 0;
        int valorI;
        double[][] rec = reconstruction(nEl, op, matU, matV, matD);
        for (int i = 0; i < nEl; i++) {
            for (int j = 0; j < nEl; j++) {
                valorI = image[i][j];
                valorR = rec[i][j];
                valor = valorI - valorR;
                if (valor < 0) {
                    valor = valor * (-1);
                }
                sum += valor;
            }
            sumt += sum;
        }
        div = 1 / (double) (nEl * nEl);
        eam = sumt * (div);
        return eam;
    }

    public static double[][] reconstruction(int nEl, int op, double matU[][], double matV[][], double matD[][]) {
        double diagonal = 0, v = 0, u = 0, sum = 0;
        double[][] rec = new double[nEl][nEl];
        for (int z = 0; z < op; z++) {
            diagonal = matD[z][z];
            for (int x = 0; x < nEl; x++) {
                v = matV[x][z];
                for (int y = 0; y < nEl; y++) {
                    u = matU[y][z];
                    sum = u * diagonal * v;
                    rec[y][x] += sum;
                }
            }
        }
        return rec;
    }

    public static void optionSave(char res, int op, int nEl, double[][] matU, double[][] matD, double[][] matV) throws FileNotFoundException {
        System.out.println("Are you sure you wish to compress the image? (Y/N)");
        res = in.next().charAt(0);
        if (res == 'Y') {
            Utilities.saveCompressed(op, nEl, matU, matD, matV);
            System.out.println("Image saved");

        }
    }

    public static int[][] printFilterInfo(int[][] image, int[][] matrix, Matrix mat, int[][] nova, char res, boolean var, int op, String fileName) throws FileNotFoundException, IOException {
        System.out.printf("%n%46s", "Smoothing");
        System.out.println("\n=======================================================================================\n");
        System.out.println("Original Matrix\n");
        Utilities.printMatrix(image);
        System.out.println("\nNew Matrix\n");
        Utilities.printMatrix(matrix);
        mat = Utilities.convertToMatrix(matrix);
        // CORRIGIR MANEIRA COMO GUARDA
        switch (op) {
            case 1:
                fileName = fileName + "Average";
                break;
            case 2:
                fileName = fileName + "Median";
                break;
            case 3:
                fileName = fileName + "Max";
                break;
            case 4:
                fileName = fileName + "Min";
                break;
            case 5:
                fileName = fileName + "Var";
                break;
            case 6:
                fileName = fileName + "Comb";
                break;
            case 7:
                fileName = fileName + "Rotation";
                break;
            case 8:
                fileName = fileName + "Sort";
                break;
        }
        nova = saving(mat, res, image, fileName, var);
        int op1 = 0;
        fileName = visualize(fileName, op1);
        return nova;
    }

    public static String visualize(String nomeFich, int op) throws IOException {
        String fileName = "New" + nomeFich + ".txt";
        if (op == 1) {
            Runtime.getRuntime().exec("gnuplot -e " + "filename='" + nomeFich + "' script.gp");
        } else {
            System.out.println("Do you wish to see the picture? (Y/N)");
            char res = in.next().charAt(0);
            if (res == 'Y') {
                Runtime.getRuntime().exec("gnuplot -e " + "filename='" + fileName + "' script.gp");
            }
        }
        return fileName;
    }

    public static String fileNameAcum(String fileName) {
        return fileName;
    }
}
