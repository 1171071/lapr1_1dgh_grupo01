package DIP;

import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.decomposition.SingularValueDecompositor;

public class Compress {

    public static Scanner in = new Scanner(System.in);

    public static void compression(int[][] image, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException {
        int op, tamanhoriginal, tamanhonova = 0, nColunaDiagonal = 1, sum = 0;
        char res = 0;
        double per;
        Matrix temp = Utilities.convertToMatrix(nova);
        SingularValueDecompositor eigenD;
        eigenD = new SingularValueDecompositor(temp);
        Locale locale = Locale.FRANCE;
        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        Matrix[] mattD = eigenD.decompose();

        double matU[][] = mattD[0].toDenseMatrix().toArray();
        double matD[][] = mattD[1].toDenseMatrix().toArray();
        double matV[][] = mattD[2].toDenseMatrix().toArray();

        System.out.println("What is the compression quality? (Insert value from 1 - " + nEl + ")");
        op = in.nextInt();
        while (op > nEl) {
            System.out.println("Value cannot be higher than matrix size");
            System.out.println("What is the compression quality? (Insert value from 1 - " + nEl + ")");
            op = in.nextInt();
        }
        tamanhoriginal = nEl * nEl;
        for (int x = 0; x < op; x++) {
            tamanhonova += (matU[x].length + matV.length + nColunaDiagonal) * 2;
        }
        System.out.println("\nThe original image requires " + tamanhoriginal + " bytes of memory space\nThe compressed image requires " + tamanhonova + " bytes of memory space.");
        double eam = Utilities.eam(nEl, image, op, matU, matV, matD);
        //Calcula percentagem de informação perdida 
        for (int i = 0; i < nEl; i++) {
            for (int j = 0; j < nEl; j++) {
                sum += image[i][j];
            }
        }
        per = (eam*100
                ) / (sum / (nEl * nEl));
        System.out.println("Lost in total " + nf.format(eam) + " of color (" + nf.format(per) + "% of color accuracy for each pixel).");
        if (tamanhoriginal < tamanhonova) {
            System.out.println("It will require " + (tamanhonova - tamanhoriginal) + " bytes more than saving the original image.");
            System.out.println("The storage size of the compressed object is higher than the original one.");
            Utilities.optionSave(res, op, nEl, matU, matD, matV);
        } else {
            System.out.println("You will gain " + (tamanhoriginal - tamanhonova) + " bytes");
            Utilities.optionSave(res, op, nEl, matU, matD, matV);
        }
    }
}
