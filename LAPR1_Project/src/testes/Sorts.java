package testes;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sorts {

    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        String fileName;
        int nEl = 0, dim = 0, op;
        int[][] image1;
        do {

            op = menu();
            switch (op) {
                case 1:
                    fileName = "testImage4.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 2:
                    fileName = "testImage40.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 3:
                    fileName = "testImage80.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 4:
                    fileName = "testImage120.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 5:
                    fileName = "testImage4160.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 6:
                    fileName = "testImage200.txt";
                    image1 = lerMatriz(nEl, dim, fileName);
                    selectionSort(image1);
                    bubbleSort(image1);
                    insertionSort(image1);
                    verMatrix(image1);
                    break;

                case 0:
                    System.out.println("FIM DO PROGRAMA");
                    break;

                default:
                    System.out.println("Introduza uma opção válida");
            }

        } while (op != 0);
    }

    public static void selectionSort(int array[][]) {
        System.out.println("======SELECTION SORT======");
        long startTime = System.nanoTime();

        for (int g = 0; g < array.length; g++) {

            for (int i = 0; i < array.length - 1; i++) {
                int pos = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j][g] > array[pos][g]) {
                        pos = j;
                    }
                }
                if (pos != i) {
                    int x = array[i][g];
                    array[i][g] = array[pos][g];
                    array[pos][g] = x;
                }
            }

        }
        /*
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ,");
            }
            System.out.println("");
        }
        
         */
        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

    }

    public static void bubbleSort(int[][] array) {
        System.out.println("======BUBBLE SORT======");
        long startTime = System.nanoTime();
        int aux;

        for (int g = 0; g < array.length; g++) {

            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length - 1; j++) {
                    if (array[j][g] < array[j + 1][g]) {
                        aux = array[j][g];
                        array[j][g] = array[j + 1][g];
                        array[j + 1][g] = aux;
                    }
                }
            }
        }
        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

        /*     for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ,");
            }
            System.out.println("");
        }*/
    }

    public static void insertionSort(int[][] array) {
        int op2;
        System.out.println("======INSERTION SORT======");
        long startTime = System.nanoTime();
        int aux;
        for (int g = 0; g < array.length; g++) {
            for (int i = 1; i < array.length; i++) {

                aux = array[i][g];
                int j = i;
                while ((j > 0) && (array[j - 1][g] < aux)) {
                    array[j][g] = array[j - 1][g];
                    j -= 1;
                }
                array[j][g] = aux;
            }
        }
        long endTime = System.nanoTime();
        long finalTime = endTime - startTime;
        System.out.println("Tempo: " + finalTime);

    }

    public static int[][] lerMatriz(int nEl, int dim, String fileName) throws FileNotFoundException {
        Scanner finput = new Scanner(new File(fileName));

        int j = 0;
        String line = finput.nextLine();

        String dimensao = "";
        int i = (line.length() - 1);

        while (line.charAt(i) != ' ') {
            dimensao = line.charAt(i) + dimensao;
            i--;
        }

        dim = Integer.parseInt(dimensao);
        nEl = 2;
        int[][] image = new int[dim][dim];
        while (finput.hasNext() && nEl < dim + 2) {
            line = finput.nextLine();
            if (line.trim().length() > 0) {
                nEl = saveInMatrix(line, image, dim, nEl, j);
                j++;
            }
        }
        finput.close();
        return image;
    }

    public static int saveInMatrix(String line, int[][] image, int dim, int nEl, int j) {

        int num = 0;
        String[] strTemp = line.split(",");
        for (int i = 0; i < strTemp.length; i++) {
            String number = strTemp[i].trim();
            num = (int) Double.parseDouble(number);
            if (num < 0 || num > 255) {
                System.out.println(num + " - Número Incorreto");
                break;
            }
            image[j][i] = num;
        }

        nEl++;
        return nEl;
    }

    public static int menu() {
        int op;
        String texto = "\n Testar matriz de ordem 4"
                + "\n2:Testar matriz de ordem 40"
                + "\n3:Testar matriz de ordem 80"
                + "\n4:Testar matriz de ordem 120"
                + "\n5:Testar matriz de ordem 160"
                + "\n6:Testar matriz de ordem 200"
                + "\n0:SAIR";
        System.out.print("\nSelect an option: ");
        op = in.nextInt();
        in.nextLine();

        return op;
    }

    public static void verMatrix(int[][] array) {
        int op2;
        do {
            System.out.println("Would you like to see the sorted matrix?\n1-YES\n2-NO");
            op2 = in.nextInt();
            in.nextLine();
            if (op2 == 1) {
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < array.length; j++) {
                        System.out.print(array[i][j] + ",");
                    }
                    System.out.println("");
                }

            } else if (op2 == 2) {

            } else {
                System.out.println("Introduza uma opção válida");
            }
        } while (op2 != 1 && op2 != 2);

    }
}
