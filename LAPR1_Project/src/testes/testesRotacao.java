package testes;

import digital.imaging.processing.Menu_3;
import digital.imaging.processing.Utilities;
import java.io.FileNotFoundException;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class testesRotacao {

    public static void rodado(int image[][], int nEl, int[][] rotacao) throws FileNotFoundException {
        boolean var = false;
        Matrix a = Utilities.convertToMatrix(image);
        int cont1 = 0;
        System.out.println("\nExpected Matrix");
        rotacao = Menu_3.rotation(a, nEl, rotacao, cont1, var);
        System.out.println("\nCalculated Matrix");
        Utilities.printMatrix(image);
        for (int i = 0; i < nEl; i++) {
            boolean rod = true;
            for (int j = 0; j < nEl; j++) {
                if (image[i][j] != rotacao[j][nEl - 1 - i]) {
                    rod = false;
                }
            }
            if (rod == true) {
                System.out.println("\nLine " + (i + 1) + " Becomes Column " + (nEl - 1 - i) + " - CORRECT");
            } else {
                System.out.println("Linha " + (i + 1) + " Becomes Column " + (nEl - 1 - i) + " - INCORRECT");
            }
        }

    }
}
