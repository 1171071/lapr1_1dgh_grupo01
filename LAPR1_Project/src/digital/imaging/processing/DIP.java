package digital.imaging.processing;

import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class DIP {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        boolean var = true;
        int op = 0, nEl = 0, dim = 0, cont1 = 0;
        String fileName = args[0];
        Scanner finput = new Scanner(new File(fileName));
        int[][] image = loadInMatrix.loading(finput, nEl, dim);
        Matrix a = Utilities.convertToMatrix(image);
        nEl = image.length;
        int[][] nova = new int[nEl][nEl];
        System.out.printf("%n%55s", "DIGITAL IMAGE PROCESSING\n");
        do {
            System.out.printf("%n%46s", "Main Menu");
            System.out.println("\n=======================================================================================\n");
            System.out.println("Please choose an operation");
            System.out.println(" 1 - Show Characteristics \n 2 - Apply Filters \n 3 - Apply Transformations \n 0 - Exit");
             System.out.print("\nSelect an option: ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    Menu_1.Statistics(image, nEl);
                    System.out.println();
                    break;
                case 2:
                    nova = Menu_2.Options(image, nEl, nova, cont1, var);
                    cont1++;
                    break;
                case 3:
                    nova = Menu_3.transformations(a, nEl, nova, cont1, var);
                    cont1++;
                    break;
                case 0:
                    System.out.println();
                    break;
                default:
                    break;
            }

        } while (op != 0);
    }
}
