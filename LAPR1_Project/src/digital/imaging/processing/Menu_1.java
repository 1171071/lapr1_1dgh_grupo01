package digital.imaging.processing;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Menu_1 {

    public static int somal = 0, somac = 0, medial = 0, mediac = 0;

    public static void Statistics(int[][] image, int nEl) {
        System.out.printf("%n%46s", "Characteristics");
        System.out.println("\n=======================================================================================\n");
        int[] suml = suml(image);
        int[] medial = medl(image, suml);
        System.out.println("=========== LINES ===========");
        for (int i = 0; i < image.length; i++) {
            System.out.println("LINHA: " + (i + 1));
            System.out.println("Soma= " + suml[i]);
            System.out.println("Média= " + medial[i] + "\n");
        }

        int[] sumc = sumc(image);
        int[] mediac = medc(image, suml);
        System.out.println("=========== COLUMNS ===========");
        for (int i = 0; i < image.length; i++) {
            System.out.println("COLUNA:" + (i + 1));
            System.out.println("Soma= " + sumc[i]);
            System.out.println("Média= " + mediac[i] + "\n    ");
        }

        int somat = sumt(suml, sumc, nEl);
        int mediat = medt(somat, nEl);
        System.out.println(
                "=========== MATRIX ===========");
        System.out.println(
                "A soma de todos os elementos é " + somat);
        System.out.println(
                "A média de todos os elementos é " + mediat + "\n");

        Matrix temp = Utilities.convertToMatrix(image);

        EigenDecompositor eigenD = new EigenDecompositor(temp);

        Matrix[] mattD = eigenD.decompose();

        System.out.println(
               "=========== EIGENVECTORS ===========");
        System.out.println(
                "Each column represents a single eigenvector \n");
        System.out.println(mattD[0]);
        System.out.println(
                "=========== EIGENVALUES ===========");
        System.out.println(
                "Each number in the diagonal represents an eigenvalue. If it has a number \nin same line, it is an imaginary number that is added to the eigenvalue \n");
        System.out.println(mattD[1]);
    }

    public static int[] suml(int[][] image) {
        somal = 0;
        int[] suml = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                somal += image[i][j];
            }
            suml[cont] = (int) somal;
            cont++;
            somal = 0;
        }

        return suml;
    }

    public static int[] sumc(int[][] image) {
        somac = 0;
        int[] sumc = new int[image.length];
        int cont = 0;

        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image.length; j++) {
                somac += image[j][i];
            }
            sumc[cont] = (int) somac;
            cont++;
            somac = 0;
        }
        return sumc;
    }

    public static int sumt(int[] suml, int[] sumc, int nEl) {
        int sumt = 0;
        for (int j = 0; j < nEl; j++) {
            sumt += suml[j] + sumc[j];
        }
        return sumt;
    }

    public static int[] medl(int[][] image, int[] suml) {
        int[] medl = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            somal = suml[cont];
            medial = (somal / image.length);
            medl[i] = medial;
            medial = 0;
            cont++;

        }
        return medl;
    }

    public static int[] medc(int[][] image, int[] sumc) {
        int[] medc = new int[image.length];
        int cont = 0;
        for (int i = 0; i < image.length; i++) {
            somac = sumc[cont];
            mediac = (somac / image.length);
            medc[i] = mediac;
            mediac = 0;
            cont++;

        }
        return medc;
    }

    public static int medt(int sumt, int nEl) {
        int mediat = 0;
        mediat = (sumt / (nEl*nEl));
        return mediat;
    }
}
