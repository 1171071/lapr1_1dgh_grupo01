package testes;

import digital.imaging.processing.loadInMatrix;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class menu {

    static Scanner ler = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        int op, nEl = 2, dim = 0;
        int[][] rotacao = new int[nEl][nEl];
        String fileName = args[0];
        Scanner finput = new Scanner(new File(fileName));
        int[][] image = loadInMatrix.loading(finput, nEl, dim);

        do {
            System.out.printf("%n%14s", "Testing Menu");
            System.out.println("\n=============================");
            do {
                System.out.println("1- Verify Stastistics");
                System.out.println("2- Verify Filters");
                System.out.println("3- Verify Rotation");
                System.out.println("4- Verify Sorting");
                System.out.println("0- Exit");

                System.out.print("\nSelect an option: ");
                op = ler.nextInt();

            } while (op < 0 || op > 4);

            switch (op) {
                case 1:
                    System.out.printf("%n%14s", "Testing Menu - Statistics");
                    System.out.println("\n=============================");
                    testes.testesEstatisticas.main(nEl, image);
                    break;
                case 2:
                    System.out.printf("%n%14s", "Testing Menu - Filters");
                    System.out.println("\n=============================");
                    testes.testesFiltros.mainFilter(image, nEl);
                    break;
                case 3:
                    System.out.printf("%n%14s", "Testing Menu - Rotation");
                    System.out.println("\n=============================");
                    testes.testesRotacao.rodado(image, nEl, rotacao);
                    break;
                case 4:
                    System.out.printf("%n%14s", "Testing Menu - Sort");
                    System.out.println("\n=============================");
                    testes.Sorts.main(args);
                    break;
                case 0:
                    break;
            }
        } while (op != 0);

    }
}
