package digital.imaging.processing;

import static digital.imaging.processing.DIP.in;
import static digital.imaging.processing.Menu_2.nomeFich;
import static digital.imaging.processing.Utilities.expandMatrix;
import static digital.imaging.processing.Utilities.saving;
import static digital.imaging.processing.Utilities.storing;
import java.io.FileNotFoundException;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Menu_2 {

    static String nomeFich = "_";

    public static int[][] Options(int[][] image, int nEl, int[][] nova, int cont1, boolean var) throws FileNotFoundException {
        int op = 0, cont = 0;
        char res = 0;
        int[][] aux = expandMatrix(image, nEl);
        int[][] matrix = new int[nEl][nEl];
        Matrix mat = Utilities.convertToMatrix(matrix);

        do {
            System.out.printf("%n%46s", "Filters");
            System.out.println("\n=======================================================================================\n");
            System.out.println("What filter do you wish to apply?");
            System.out.println(" 1 - Smoothing (Average) \n 2 - Noise Reduction (Median) \n 3 - Intensify Bright Regions (Maximum) \n 4 - Intensify Dark Regions (Minimum) \n 0 - Exit");
            System.out.print("\nSelect an option: ");
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = average(aux, nEl);
                            System.out.printf("%n%46s", "Smoothing");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);

                            nomeFich = nomeFich + "Average";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = average(nova, nEl);
                            System.out.printf("%n%46s", "Smoothing");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Average";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                    }
                    matrix = average(aux, nEl);
                    System.out.printf("%n%46s", "Smoothing");
                    System.out.println("\n=======================================================================================\n");
                    System.out.println("Original Matrix\n");
                    Utilities.printMatrix(image);
                    System.out.println("\nNew Matrix\n");
                    Utilities.printMatrix(matrix);
                    nomeFich = nomeFich + "Average";
                    nova = saving(mat, res, cont, image, nomeFich, var);
                    break;
                case 2:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = mediana(aux, nEl);
                            System.out.printf("%n%46s", "Noise Reduction");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Median";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = mediana(nova, nEl);
                            System.out.printf("%n%46s", "Noise Reduction");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Median";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                    }
                    matrix = mediana(aux, nEl);
                    System.out.printf("%n%46s", "Noise Reduction");
                    System.out.println("\n=======================================================================================\n");
                    System.out.println("Original Matrix\n");
                    Utilities.printMatrix(image);
                    System.out.println("\nNew Matrix\n");
                    Utilities.printMatrix(matrix);
                    nomeFich = nomeFich + "Median";
                    nova = saving(mat, res, cont, image, nomeFich, var);
                    break;
                case 3:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = maxnmin(aux, nEl, op);
                            System.out.printf("%n%55s", "Intensify Bright Regions");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Maximum";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = maxnmin(nova, nEl, op);
                            System.out.printf("%n%55s", "Intensify Bright Regions");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Maximum";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                    }
                    matrix = maxnmin(aux, nEl, op);
                    System.out.printf("%n%55s", "Intensify Bright Regions");
                    System.out.println("\n=======================================================================================\n");
                    System.out.println("Original Matrix\n");
                    Utilities.printMatrix(image);
                    System.out.println("\nNew Matrix\n");
                    Utilities.printMatrix(matrix);
                    nomeFich = nomeFich + "Maximum";
                    nova = saving(mat, res, cont, image, nomeFich, var);
                    break;
                case 4:
                    if (cont != 0 || cont1 != 0) {
                        System.out.println("Do you wish to apply the filter on the original image? (Y/N)");
                        res = in.next().charAt(0);
                        if (res == 'Y') {
                            matrix = maxnmin(aux, nEl, op);
                            System.out.printf("%n%55s", "Intensify Dark Regions");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Minimum";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                        if (res == 'N') {
                            nova = expandMatrix(nova, nEl);
                            matrix = maxnmin(nova, nEl, op);
                            System.out.printf("%n%55s", "Intensify Dark Regions");
                            System.out.println("\n=======================================================================================\n");
                            System.out.println("Original Matrix\n");
                            Utilities.printMatrix(image);
                            System.out.println("\nNew Matrix\n");
                            Utilities.printMatrix(matrix);
                            nomeFich = nomeFich + "Minimum";
                            nova = saving(mat, res, cont, image, nomeFich, var);
                            break;
                        }
                    }
                    matrix = maxnmin(aux, nEl, op);
                    System.out.printf("%n%55s", "Intensify Dark Regions");
                    System.out.println("\n=======================================================================================\n");
                    System.out.println("Original Matrix\n");
                    Utilities.printMatrix(image);
                    System.out.println("\nNew Matrix\n");
                    Utilities.printMatrix(matrix);
                    nomeFich = nomeFich + "Minimum";
                    nova = saving(mat, res, cont, image, nomeFich, var);
                    break;
                case 0:
                    System.out.println();
                    break;
                default:
                    break;
            }
            cont++;
        } while (op != 0);
        return nova;
    }

    public static int[][] average(int[][] aux, int nEl) {
        int sum = 0;
        int avg = 0;
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b;
        int[][] average = new int[nEl][nEl];
        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux[i].length - 1; j++) {
                a = i;
                b = j;
                do {
                    x = i - 1;
                    do {
                        y = j - 1;
                        sum = sum + aux[x][y];
                        cont++;
                        j++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    i++;
                    j = b;
                } while (cont1 < 3);
                cont1 = 0;
                i = a;
                j = b;

                avg = sum / 9;
                average[i - 1][j - 1] = avg;
                sum = 0;
                avg = 0;
            }

        }
        return average;
    }

    public static int[][] mediana(int[][] aux, int nEl) {
        int[] med = new int[9];
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b, q = 0;
        int[][] mediana = new int[nEl][nEl];
        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux[i].length - 1; j++) {
                a = i;
                b = j;
                do {
                    x = i - 1;
                    do {
                        y = j - 1;
                        med[q] = aux[x][y];
                        q++;
                        cont++;
                        j++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    i++;
                    j = b;

                } while (cont1 < 3);
                cont1 = 0;
                i = a;
                j = b;
                for (int l = 0; l < 8; l++) {
                    for (int p = 0; p < 8; p++) {
                        if (med[p] > med[p + 1]) {
                            double t = med[p];
                            med[p] = med[p + 1];
                            med[p + 1] = (int) t;
                        }
                    }
                }
                mediana[i - 1][j - 1] = (int) med[4];
                q = 0;
            }
        }
        return mediana;
    }

    public static int[][] maxnmin(int[][] aux, int nEl, int op) {
        int[] arr = new int[9];
        double[][] array = new double[nEl][nEl];
        int[][] maxmin = new int[nEl][nEl];
        int cont = 0, cont1 = 0, x = 0, y = 0, a, b, q = 0;
        Matrix mat = new Basic2DMatrix(array);

        for (int i = 1; i < aux.length - 1; i++) {
            for (int j = 1; j < aux.length - 1; j++) {
                a = i;
                b = j;
                x = i - 1;
                do {

                    y = j - 1;
                    do {
                        arr[q] = aux[x][y];
                        q++;
                        cont++;
                        y++;
                    } while (cont < 3);
                    cont1++;
                    cont = 0;
                    x++;
                    y = b;
                } while (cont1 < 3);
                cont1 = 0;
                x = a;
                y = b;
                for (int l = 0; l < 8; l++) {
                    for (int p = 0; p < 8; p++) {
                        if (arr[p] > arr[p + 1]) {
                            double t = arr[p];
                            arr[p] = arr[p + 1];
                            arr[p + 1] = (int) t;
                        }
                    }
                }
                mat = storing(op, array, arr, nEl, i, j, mat);
                double[][] maxminD = mat.toDenseMatrix().toArray();
                for (int t = 0; t < maxminD.length; t++) {
                    for (int u = 0; u < maxminD.length; u++) {
                        maxmin[t][u] = (int) maxminD[t][u];
                    }
                }
                q = 0;
            }
        }
        return maxmin;
    }

}
